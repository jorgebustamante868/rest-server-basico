const {response} = require('express')

const usuariosGet = (req, res = response) => {

    const query = req.query;
    res.json({
        ok : true,
        msg: 'get Api - Controlador',
        query
    })
}

const usuariosPost = (req, res = response) => {

    const {nombre , edad}= req.body;
    // console.log("HOLA MUNDO " + JSON.stringify(body))
    res.json({
        ok : true,
        msg: 'get Post - Controlador', 
        nombre,
        edad,
        anio:'2022'
    })
}

const usuariosDelete = (req, res = response) => {
    res.json({
        ok : true,
        msg: 'get Delete - Controlador'
    })
}

const usuariosPut = (req, res = response) => {

    const id = req.params.id;

    res.json({
        ok : true,
        msg: 'get Put - Controlador',
        id
    })
}

const usuariosPatch = (req, res = response) => {
    res.json({
        ok : true,
        msg: 'get Patch - Controlador'
    })
}


module.exports = {
    usuariosGet ,
    usuariosPost ,
    usuariosDelete ,
    usuariosPut ,
    usuariosPatch
}